from django.http.response import HttpResponse

import simpel_hookup as hookup


def index(request):
    text = "Lorem ipsum dolor sit amet"

    hook_funcs = hookup.get_hooks('PROCESS_TEXT_HOOKS')
    for func in hook_funcs:
        text = func(text)

    return HttpResponse(text)
