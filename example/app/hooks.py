import simpel_hookup as hookup


@hookup.register("PROCESS_TEXT_HOOKS", order=1)
def process_text_replace_space(text):
    text = text.replace(" ", "-")
    print(text)
    return text


@hookup.register("PROCESS_TEXT_HOOKS", order=2)
def process_text_replace_dash(text):
    text = text.replace("-", "_")
    print(text)
    return text
