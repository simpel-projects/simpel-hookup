from simpel_hookup.version import get_version

from .core import *  # NOQA

VERSION = (0, 1, 0, "final", 0)

__version__ = get_version(VERSION)
